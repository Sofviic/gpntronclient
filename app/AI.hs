module AI where

import Types
import Utils

data State = State {
        pid :: PID,
        posX :: X,
        posY :: Y,
        sWidth :: Width,
        sHeight :: Height,
        occupied :: [(PID,X,Y)]
        } deriving (Show,Eq)

addPos :: Packet -> State -> State
addPos (Pos ppid x y) s = s{occupied=(ppid,x,y) : occupied s}

whereShouldIgo :: State -> Direction
whereShouldIgo s = fst
                . headS (DirDown,True) 
                . filter snd 
                . zip [DirDown,DirUp,DirRight,DirLeft]
                $ not . isOccupied' s 
                <$> (both2 (flip mod) (sWidth s,sHeight s))
                <$> ($ bimap (posX,posY) s) 
                <$> ([fmap,first] <*> [succ,pred])

removeIDs :: [PID] -> State -> State
removeIDs pids s = s{occupied= filter (\(p,_,_) -> not $ elem p pids) $ occupied s}

isOccupied :: X -> Y -> State -> Bool
isOccupied x y s = (>0) . length . filter (\(_,i,j) -> (i,j) == (x,y)) $ occupied s

isOccupied' :: State -> (X,Y) -> Bool
isOccupied' s (x,y) = isOccupied x y s

showOccupiedState :: State -> String
showOccupiedState = unlines . showOccupiedState'

showOccupiedState' :: State -> [[Char]]
showOccupiedState' s = [[if' (isOccupied x y s) '*' ' '| x <- [0..sWidth s]]| y <- [0..sHeight s]]
