{-# LANGUAGE OverloadedStrings #-}
module Main (main) where

import Types
import AI
import Utils
import Control.Monad
import System.Random
import Data.ByteString hiding (zip,filter,length,head,elem)
import Network.Simple.TCP

{--
Worm dies when intersecting, including 2 worms going to the same square on the same turn
Startagies to explore:
- store state of map with worm heads
- dont go to deadends (extend to $n$-lookahead)
- avoid going to a cell that another worm may go to
- block other worms by being inefficent with space in contested areas
- spiral path; only allow yourself to use the space
- see if you can focus on killing a worm if it allows you to have better positioning
- focus on killing higher elo bots (remember usernames)
- etc...

-- also: create a manual control client.

--}

main :: IO ()
main = connect "gpn-tron.metamuffin.org" "4000" client

client :: (Socket, SockAddr) -> IO ()
client (s, _) = do
        recvPacket s >>= handleMOTD
        sendPacket s $ Join "sof2" "idk"
        iterateM_ newState $ (recvPacket s >>=) . handlePacket s

clientDebug :: (Socket, SockAddr) -> IO ()
clientDebug (s, remoteAddr) = do
        putStrLn $ "Connection established to " <> show remoteAddr
        recvPacket s >>= handleMOTD
        sendPacket s $ Join "sof2" "idk"
        iterateM_ newState $ liftM2 (>>) 
            (putStrLn . showOccupiedState) 
            ((recvPacket s >>=) . handlePacket s)

handleMOTD :: Maybe Packet -> IO ()
handleMOTD (Just (Motd s)) = putStrLn $ "*Massage* Of The Day: " <> s
handleMOTD _ = error "AAAAAAAAAAAAAAAAAAAAAAAAAA"

handlePacket :: Socket -> State -> Maybe Packet -> IO State
handlePacket s state (Just Tick) = do 
                                    doOnProb 10 . sendPacket s . Chat $ "y do i keep killing myself"
                                    liftM2 (>>) (sendPacket s . Move . whereShouldIgo) return state
handlePacket _ state (Just (Game w h myPid)) = return state{pid=myPid,sWidth=w,sHeight=h}
handlePacket _ state (Just pos@(Pos ppid x y)) | pid state == ppid = return . addPos pos $ state{posX=x, posY=y}
handlePacket _ state (Just pos@(Pos ppid _ _)) | pid state /= ppid = return . addPos pos $ state
handlePacket _ state (Just (Die pids)) = return $ removeIDs pids state
handlePacket _ _ (Just (Win _ _)) = return newState
handlePacket _ _ (Just (Lose _ _)) = return newState
handlePacket _ state mp = (putStrLn . show) mp >> return state

-- in integer percentages
doOnProb :: Int -> IO () -> IO ()
doOnProb p m = (randomRIO (0,99) :: IO Int) >>= \q -> if' (q < p) m mempty

recvPacket :: Socket -> IO (Maybe Packet)
recvPacket s = (=<<) sPacket <$> recvLine s

-- TODO: check if rewrite using iterateMaybeM is possible
recvLine :: Socket -> IO (Maybe ByteString)
recvLine s = do
    mb <- recv s 1
    case mb of
        Nothing   -> return $ Nothing
        Just "\n" -> return $ Just ""
        Just b    -> fmap (b<>) <$> recvLine s

newState :: State
newState = State 0 0 0 0 0 []

sendPacket :: Socket -> Packet -> IO ()
sendPacket s = send s . cPacket


