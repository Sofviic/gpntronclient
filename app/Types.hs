module Types where


import Text.Read
import Data.List.Split
import qualified Data.ByteString.Char8 as B

type Username = String
type Password = String
type PID = Int
type Width = Int
type Height = Int
type X = Int
type Y = Int
type Wins = Int
type Losses = Int
data Direction = DirUp 
               | DirRight 
               | DirDown 
               | DirLeft
               deriving (Eq,Show)

data Packet = Motd String
            | Join Username Password --client
            | Error String
            | Game Width Height PID
            | Pos PID X Y
            | Player PID Username
            | Tick
            | Die [PID]
            | Move Direction --client
            | Chat String --client
            | Message PID String
            | Win Wins Losses
            | Lose Wins Losses
            deriving (Eq,Show)

-------server
parsePacket :: String -> Maybe Packet
parsePacket s = case arg !! 0 of
    "motd"      -> return $ Motd     (arg !! 1)
    "join"      -> return $ Join     (arg !! 1)(arg !! 2)
    "error"     -> return $ Error    (arg !! 1)
    "game"      -> parsePacketGame arg
    "pos"       -> parsePacketPos arg
    "player"    -> parsePacketPlayer arg
    "tick"      -> return $ Tick
    "die"       -> parsePacketDie arg
    "move"      -> parsePacketMove arg
    "chat"      -> return $ Chat     (arg !! 1)
    "message"   -> parsePacketMessage arg
    "win"       -> parsePacketWin arg
    "lose"      -> parsePacketLose arg
    otherwise   -> Nothing
    where arg = splitOn "|" s

parsePacketGame :: [String] -> Maybe Packet
parsePacketGame (_:mw:mh:mpid:_) = do
    w <- readMaybe mw
    h <- readMaybe mh
    pid <- readMaybe mpid
    return $ Game w h pid
parsePacketGame _ = Nothing

parsePacketPos :: [String] -> Maybe Packet
parsePacketPos (_:mpid:mx:my:_) = do
    pid <- readMaybe mpid
    x <- readMaybe mx
    y <- readMaybe my
    return $ Pos pid x y
parsePacketPos _ = Nothing

parsePacketPlayer :: [String] -> Maybe Packet
parsePacketPlayer (_:mpid:username:_) = do
    pid <- readMaybe mpid
    return $ Player pid username
parsePacketPlayer _ = Nothing

parsePacketDie :: [String] -> Maybe Packet
parsePacketDie (_:mpids) = fmap Die . sequence $ readMaybe <$> mpids
parsePacketDie _ = Nothing

parseDir :: String -> Maybe Direction
parseDir "up"   = Just DirUp
parseDir "right"= Just DirRight
parseDir "down" = Just DirDown
parseDir "left" = Just DirLeft
parseDir _ = Nothing

showDir :: Direction -> String
showDir DirUp      = "up"
showDir DirRight   = "right"
showDir DirDown    = "down"
showDir DirLeft    = "left"

parsePacketMove :: [String] -> Maybe Packet
parsePacketMove (_:mdir:_) = Move <$> parseDir mdir
parsePacketMove _ = Nothing

parsePacketMessage :: [String] -> Maybe Packet
parsePacketMessage (_:mpid:msg:_) = do
    pid <- readMaybe mpid
    return $ Message pid msg 
parsePacketMessage _ = Nothing

parsePacketWin :: [String] -> Maybe Packet
parsePacketWin (_:mwin:mlose:_) = do
    win <- readMaybe mwin
    lose <- readMaybe mlose
    return $ Win win lose 
parsePacketWin _ = Nothing

parsePacketLose :: [String] -> Maybe Packet
parsePacketLose (_:mwin:mlose:_) = do
    win <- readMaybe mwin
    lose <- readMaybe mlose
    return $ Lose win lose 
parsePacketLose _ = Nothing

sPacket :: B.ByteString -> Maybe Packet
sPacket = parsePacket . B.unpack

-------client
showJoin :: Packet -> String
showJoin (Join username password) = "join|" <> username <> "|" <> password

showChat :: Packet -> String
showChat (Chat msg) = "chat|" <> msg

showMove :: Packet -> String
showMove (Move dir) = "move|" <> showDir dir

cPacket :: Packet -> B.ByteString
cPacket p@(Join _ _) = B.pack . (<>"\n") $ showJoin p
cPacket p@(Chat _  ) = B.pack . (<>"\n") $ showChat p
cPacket p@(Move _  ) = B.pack . (<>"\n") $ showMove p
