module Utils where

import Utils.Compose

first :: (a -> b) -> (a,c) -> (b,c)
first f (a,b) = (f a,b)

bimap :: (a -> b, a -> c) -> a -> (b,c)
bimap (f,g) a = (f a, g a)

both2 :: (a -> b -> c) -> (a,a) -> (b,b) -> (c,c)
both2 f (a,b) (c,d) = (f a c, f b d)

iterateM_ :: Monad m => a -> (a -> m a) -> m ()
iterateM_ = (>> pure ()) .#. iterateM

iterateM :: Monad m => a -> (a -> m a) -> m [a]
iterateM x f = f x >>= \r -> (r:) <$> iterateM r f

iterateMaybeM :: Monad m => a -> (a -> m (Maybe a)) -> m [a]
iterateMaybeM x f = f x >>= \r -> case r of
    Nothing -> pure []
    Just x -> (x:) <$> iterateMaybeM x f

headS :: a -> [a] -> a
headS a [] = a
headS _ (a:_) = a

if' :: Bool -> a -> a -> a
if' p a b = if p then a else b
